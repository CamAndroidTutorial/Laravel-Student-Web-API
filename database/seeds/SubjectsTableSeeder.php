<?php

use Illuminate\Database\Seeder;
use App\Subject;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subject::truncate();
        $faker = \Faker\Factory::create();

        for ($i=0;$i<10;$i++){
            Subject::create([
                'subject_title' => $faker->company,
            ]);
        }
    }
}
