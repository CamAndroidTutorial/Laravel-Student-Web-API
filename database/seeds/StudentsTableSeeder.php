<?php

use Illuminate\Database\Seeder;
use App\Student;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Student::truncate();
        $faker = \Faker\Factory::create();

        for ($i=0;$i<50;$i++){
            Student::create([
                'student_name' => $faker->name,
                'student_gender' => 'male',
                'age' => $faker->numberBetween(10, 60)
            ]);
        }
    }
}
